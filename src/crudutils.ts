import { Action } from "redux"
import { AxiosResponse } from "axios"
import { ThunkDispatch } from "redux-thunk"

export interface IdMatchParams {
  id: string | undefined;
}

export function strToInt(idStr: string | undefined): number | null {
  if (idStr) {
    return parseInt(idStr);
  }
  return null;
}

export interface Fault {
  message: string
  error: { [arg: string]: string } | Error | null
}

export interface CrudAction<T, D> extends Action<T> {
  type: T
  payload: D
  redirectTo: string | null
}

export function handleResponse<T, ET, D, S, E>(
  response: AxiosResponse<D>,
  dispatch: ThunkDispatch<S, E, CrudAction<T, D | Fault>>,
  type: T,
  errorType: ET,
  message: string,
  redirectOnSuccessTo: string | null,
  alternatePayload: D | null = null
): void {
  try {
    if (response.status >= 200 && response.status <= 299) {
      const action = {
        type: type,
        payload: alternatePayload ? alternatePayload : response.data,
        redirectTo: redirectOnSuccessTo
      }
      dispatch(action)
    } else {
      dispatch({
        type: type,
        payload: {
          message: message,
          error: { http_statu: response.status.toString() }
        },
        redirectTo: null
      })
    }
  } catch (e) {
    dispatch({
      type: type,
      payload: {
        message: message,
        error: e
      },
      redirectTo: null
    })
  }
}
