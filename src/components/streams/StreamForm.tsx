import React from "react";
import {
  Field,
  FormErrors,
  InjectedFormProps,
  reduxForm,
  WrappedFieldMetaProps,
  WrappedFieldProps,
} from "redux-form";
import {
  StreamDispatchProps,
  StreamFormProps,
} from "../../reducers/streamsReducer";

interface CustomFieldPropsLabel {
  label: string;
}

class StreamForm extends React.Component<
  StreamDispatchProps & InjectedFormProps<StreamFormProps, StreamDispatchProps>
> {
  renderError = ({ error, touched }: WrappedFieldMetaProps) => {
    if (touched && error) {
      return (
        <div className="ui error message">
          <div className="header">{error}</div>
        </div>
      );
    }
  };

  renderInput = ({
    input,
    label,
    meta,
  }: WrappedFieldProps & CustomFieldPropsLabel) => {
    const className = `field ${meta.error && meta.touched ? "error" : ""}`;
    return (
      <div className={className}>
        <label>{label}</label>
        <input {...input} />
        {this.renderError(meta)}
      </div>
    );
  };
  onSubmit = (formValues: StreamFormProps) => {
    this.props.onSubmit(formValues);
  };
  render() {
    return (
      <form
        onSubmit={this.props.handleSubmit(this.onSubmit)}
        className="ui form error"
      >
        <Field name="title" component={this.renderInput} label="Enter Title" />
        <Field
          name="description"
          component={this.renderInput}
          label="Enter Description"
        />
        <button className="ui button primary">Submit</button>
      </form>
    );
  }
}

const validate = (formValues: StreamFormProps): FormErrors<StreamFormProps> => {
  const errors: FormErrors<StreamFormProps> = {};
  if (!formValues.title) {
    errors.title = "You must enter a title";
  }
  if (!formValues.description) {
    errors.description = "You must enter a description";
  }
  return errors;
};

export default reduxForm<StreamFormProps, StreamDispatchProps>({
  form: "streamForm",
  validate,
})(StreamForm);
