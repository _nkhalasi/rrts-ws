import React from "react";
import { Stream } from "../../reducers/streamsReducer";
import { ApplicationState } from "../../reducers";
import { RouteComponentProps } from "react-router";
import { IdMatchParams, strToInt } from "../../crudutils";
import { connect, ConnectedProps } from "react-redux";
import { deleteStream, fetchStream } from "../../actions/streams";
import Modal from "../Modal";
import { Link } from "react-router-dom";

export interface StreamDeleteDispatchProps {
  fetchStream: (id: number) => void;
  deleteStream: (id: number) => void;
}

export interface StreamDeleteDomainProps {
  stream: Stream | null;
}

interface StreamDeleteState {
  id: number | null;
}

class StreamDelete extends React.Component<
  StreamDeleteProps,
  StreamDeleteState
> {
  constructor(props: StreamDeleteProps, state: StreamDeleteState) {
    super(props, state);
    const id = strToInt(this.props.match.params.id);
    this.state = { id };
  }

  componentDidMount() {
    if (this.state.id != null) this.props.fetchStream(this.state.id);
  }

  renderContent() {
    if (!this.props.stream) {
      return "Could not locate stream";
    }
    return `Are you sure you want to delete stream ${this.props.stream.title}`;
  }

  renderActions() {
    const id = this.state.id;
    if (id != null) {
      return (
        <React.Fragment>
          <button
            onClick={() => this.props.deleteStream(id)}
            className="ui button negative"
          >
            Delete
          </button>
          <Link to="/" className="ui button">
            Cancel
          </Link>
        </React.Fragment>
      );
    } else {
      return <div>Invalid argument passed</div>;
    }
  }

  render() {
    const id = this.state.id;
    if (this.props.stream) {
      return (
        <Modal
          title="Delete Stream"
          content={this.renderContent()}
          actions={this.renderActions()}
          onDismiss={() => {
            this.props.history.push("/");
          }}
        />
      );
    } else {
      return (
        <div>{`No stream found for id ${this.props.match.params.id}`}</div>
      );
    }
  }
}

const mapStateToProps = (
  state: ApplicationState,
  ownProps: StreamDeleteDomainProps &
    StreamDeleteDispatchProps &
    RouteComponentProps<IdMatchParams>
) => {
  const id = strToInt(ownProps.match.params.id);
  if (id != null) return { stream: state.streams.items[id] };
  else return null;
};

const connector = connect(mapStateToProps, { fetchStream, deleteStream });
type StreamDeleteProps = ConnectedProps<typeof connector> &
  StreamDeleteDomainProps &
  StreamDeleteDispatchProps &
  RouteComponentProps<IdMatchParams>;

export default connector(StreamDelete);
