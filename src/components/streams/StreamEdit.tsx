import React from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { Stream, StreamFormProps } from "../../reducers/streamsReducer";
import { ApplicationState } from "../../reducers";
import { RouteComponentProps } from "react-router";
import { IdMatchParams, strToInt } from "../../crudutils";
import { editStream, fetchStream } from "../../actions/streams";
import { ConnectedProps } from "react-redux";
import StreamForm from "./StreamForm";

export interface StreamEditDispatchProps {
  fetchStream: (id: number) => void;
  editStream: (id: number, formValues: StreamFormProps) => void;
}

export interface StreamEditDomainProps {
  stream: Stream | null;
}

export interface StreamEditState {
  id: number | null;
}

class StreamEdit extends React.Component<StreamEditProps, StreamEditState> {
  constructor(props: StreamEditProps, state: StreamEditState) {
    super(props, state);
    const id = strToInt(this.props.match.params.id);
    this.state = { id };
  }

  componentDidMount() {
    if (this.state.id != null) this.props.fetchStream(this.state.id);
  }

  onSubmit = (formValues: StreamFormProps) => {
    const id = this.state.id;
    if (id != null) this.props.editStream(id, formValues);
  };

  render() {
    if (!this.props.stream) {
      return <div>Loading ...</div>;
    }
    return (
      <div>
        <h3>Edit a Stream</h3>
        <StreamForm
          initialValues={_.pick(this.props.stream, "title", "description")}
          onSubmit={this.onSubmit}
        />
      </div>
    );
  }
}

const mapStateToProps = (
  state: ApplicationState,
  ownProps: StreamEditDomainProps &
    StreamEditDispatchProps &
    RouteComponentProps<IdMatchParams>
) => {
  const id = strToInt(ownProps.match.params.id);
  if (id != null) return { stream: state.streams.items[id] };
  else return null;
};
const connector = connect(mapStateToProps, { fetchStream, editStream });
type StreamEditProps = ConnectedProps<typeof connector> &
  StreamEditDomainProps &
  StreamEditDispatchProps &
  RouteComponentProps<IdMatchParams>;
export default connector(StreamEdit);
