import React from "react";
import StreamForm from "./StreamForm";
import { formValues } from "redux-form";
import { connect, ConnectedProps } from "react-redux";

import {
  StreamDispatchProps,
  StreamFormProps,
} from "../../reducers/streamsReducer";
import { createStream } from "../../actions/streams";
import { RouteComponentProps } from "react-router";

class StreamCreate extends React.Component<StreamCreateProps> {
  onSubmit = (formValues: StreamFormProps) => {
    this.props.createStream(formValues);
  };

  render() {
    const dispatchProps: StreamDispatchProps = { onSubmit: this.onSubmit };
    return (
      <div>
        <h3>Create a Stream</h3>
        <StreamForm {...dispatchProps} />
      </div>
    );
  }
}

const connector = connect(null, { createStream });
type StreamCreateProps = ConnectedProps<typeof connector> & RouteComponentProps;
export default connector(StreamCreate);
