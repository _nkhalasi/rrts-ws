import React from "react";
import { Stream } from "../../reducers/streamsReducer";
import { ApplicationState } from "../../reducers";
import { RouteComponentProps } from "react-router";
import { IdMatchParams, strToInt } from "../../crudutils";
import { connect, ConnectedProps } from "react-redux";
import { fetchStream } from "../../actions/streams";

export interface StreamFetchDispatchProps {
  fetchStreams: (id: number) => void;
}

export interface StreamFetchDomainProps {
  stream: Stream | null;
}

export interface StreamFetchState {
  id: number | null;
}

class StreamFetch extends React.Component<StreamFetchProps, StreamFetchState> {
  constructor(props: StreamFetchProps, state: StreamFetchState) {
    super(props, state);
    const id = strToInt(this.props.match.params.id);
    this.state = { id };
  }

  componentDidMount() {
    console.log(`StreamFetch mounted for ${this.state.id}`);
    if (this.state.id != null) this.props.fetchStream(this.state.id);
  }

  render() {
    console.log(this.props);
    if (!this.props.stream) {
      return <div>Loading ...</div>;
    }
    const { title, description } = this.props.stream;

    return (
      <div>
        <h3>{title}</h3>
        <h5>{description}</h5>
      </div>
    );
  }
}

const mapStateToProps = (
  state: ApplicationState,
  ownProps: StreamFetchDomainProps &
    StreamFetchDispatchProps &
    RouteComponentProps<IdMatchParams>
) => {
  const id = strToInt(ownProps.match.params.id);
  if (id != null) return { stream: state.streams.items[id] };
  else return null;
};

const connector = connect(mapStateToProps, { fetchStream });
type StreamFetchProps = ConnectedProps<typeof connector> &
  StreamFetchDomainProps &
  StreamFetchDispatchProps &
  RouteComponentProps<IdMatchParams>;

export default connector(StreamFetch);
