import React from "react";
import { Link, RouteComponentProps } from "react-router-dom";
import { ApplicationState } from "../../reducers";
import { connect, ConnectedProps } from "react-redux";
import { Stream } from "../../reducers/streamsReducer";
import { listStreams } from "../../actions/streams";

// const listStreams = () => {
//   return {
//     type: StreamActionTypes.List,
//     payload: [
//       {
//         id: 1,
//         title: "Hello World",
//         description: "Greetings Martians",
//         userId: "104851314699965724473",
//       },
//     ],
//   };
// };

class StreamList extends React.Component<StreamListProps> {
  componentDidMount() {
    this.props.listStreams();
  }

  renderAdmin(stream: Stream) {
    if (stream.userId === this.props.currentUserId) {
      return (
        <div className="right floated content">
          <Link to={`streams/edit/${stream.id}`} className="ui button primary">
            Edit
          </Link>
          <Link
            to={`streams/delete/${stream.id}`}
            className="ui button negative"
          >
            Delete
          </Link>
        </div>
      );
    }
  }

  renderList() {
    if (this.props.isSignedIn) {
      return this.props.streams.map((stream: Stream) => {
        return (
          <div className="item" key={stream.id}>
            {this.renderAdmin(stream)}
            <i className="large middle aligned icon camera"></i>
            <div className="content">
              <Link to={`/streams/show/${stream.id}`} className="header">
                {stream.title}
              </Link>
              <div className="description">{stream.description}</div>
            </div>
          </div>
        );
      });
    }
  }

  renderCreate() {
    if (this.props.isSignedIn) {
      return (
        <div>
          <Link to={`streams/new`} className="ui button primary">
            CreateStream
          </Link>
        </div>
      );
    }
  }

  render() {
    return (
      <div>
        <h2>Streams</h2>
        <div className="ui celled list">{this.renderList()}</div>
        {this.renderCreate()}
      </div>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => {
  console.log(state);
  return {
    streams: Object.values(state.streams.items),
    currentUserId: state.auth.userId,
    isSignedIn: state.auth.isSignedIn,
  };
};

const connector = connect(mapStateToProps, { listStreams });
type StreamListProps = ConnectedProps<typeof connector> &
  RouteComponentProps & {};

export default connector(StreamList);
