import React from "react";
import { signIn, signOut, AuthActions } from "../actions/auth";
import { ApplicationState } from "../reducers";
import { connect } from "react-redux";

const dummyAction = () => {
  return {
    type: AuthActions.Dummy,
  };
};

interface GoogleAuthProps {
  isSignedIn: boolean | null;
  signIn: typeof signIn;
  signOut: typeof signOut;
  dummy: typeof dummyAction;
}

class GoogleAuth extends React.Component<GoogleAuthProps> {
  auth: gapi.auth2.GoogleAuth | undefined;
  componentDidMount() {
    window.gapi.load("client:auth2", () => {
      window.gapi.client
        .init({
          clientId:
            // "626721383523-iqj53un78bnovsqbe2qjh11rsuhq12rk.apps.googleusercontent.com",
            "495217473546-3m0pu8jvim2h5ia8tbnd44nuv06g550r.apps.googleusercontent.com",
          scope: "email",
        })
        .then(() => {
          this.auth = window.gapi.auth2.getAuthInstance();
          this.onAuthChange(this.auth.isSignedIn.get());
          this.auth.isSignedIn.listen(this.onAuthChange);
        });
    });
  }

  onAuthChange = (isSignedIn: boolean) => {
    if (this.auth && isSignedIn) {
      this.props.signIn(this.auth.currentUser.get().getId());
    } else {
      this.props.signOut();
    }
  };

  onSignInClick = () => {
    if (this.auth) {
      this.auth.signIn();
    }
  };
  onSignOutClick = () => {
    if (this.auth) {
      this.auth.signOut();
    }
  };
  renderAuthButton() {
    if (this.props.isSignedIn) {
      return (
        <button className="ui green button" onClick={this.onSignOutClick}>
          <i className="google icon" />
          Sign Out
        </button>
      );
    } else {
      return (
        <button className="ui red button" onClick={this.onSignInClick}>
          <i className="google icon" />
          Sign In With Google
        </button>
      );
    }
  }

  render() {
    console.log(`Rendering with state=${this.props.isSignedIn}`);
    return <div>{this.renderAuthButton()}</div>;
  }
}

const mapStateToProps = (state: ApplicationState) => {
  return { isSignedIn: state.auth.isSignedIn };
};

export default connect(mapStateToProps, {
  dummy: dummyAction,
  signIn,
  signOut,
})(GoogleAuth);
