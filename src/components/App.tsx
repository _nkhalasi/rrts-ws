import React from "react";
import { connect, ConnectedProps } from "react-redux";
import Header from "./Header";
import StreamList from "./streams/StreamList";
import StreamCreate from "./streams/StreamCreate";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";
import { ApplicationState } from "../reducers";
import { RouterProps } from "react-router";
import StreamEdit from "./streams/StreamEdit";
import StreamDelete from "./streams/StreamDelete";
import StreamFetch from "./streams/StreamFetch";

interface AppDomainProps {
  redirectTo: string | null;
}
class App extends React.Component<AppProps> {
  renderRedirect() {
    if (
      this.props &&
      this.props.redirectTo != null &&
      this.props.redirectTo !== window.location.pathname
    ) {
      this.forceUpdate();
      return (
        <Router>
          <Redirect to={this.props.redirectTo} />
        </Router>
      );
    } else {
      return null;
    }
  }
  render() {
    return (
      this.renderRedirect() || (
        <div className="ui container">
          <Router>
            <Header />
            <Switch>
              <Route path="/" exact component={StreamList} />
              <Route path="/streams" exact component={StreamList} />
              <Route path="/streams/new" exact component={StreamCreate} />
              <Route path="/streams/edit/:id" exact component={StreamEdit} />
              <Route
                path="/streams/delete/:id"
                exact
                component={StreamDelete}
              />
              <Route path="/streams/show/:id" exact component={StreamFetch} />
            </Switch>
          </Router>
        </div>
      )
    );
  }
}

const mapStateToProps = (state: ApplicationState, ownProps: AppDomainProps) => {
  return { redirectTo: state.streams.redirectTo };
};

const connector = connect(mapStateToProps, {});
type AppProps = ConnectedProps<typeof connector> & AppDomainProps;
export default connector(App);
