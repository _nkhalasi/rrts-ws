import _ from "lodash";
import { AllStreamActions, StreamActionTypes } from "../actions/streams";

export interface Stream {
  id: number;
  title: string;
  description: string;
  userId: string;
}

export interface Streams {
  [id: number]: Stream;
}

export interface StreamsState {
  items: Streams;
  loading: boolean;
  error: String | null;
  redirectTo: string | null;
}

export interface StreamDispatchProps {
  onSubmit: (p: StreamFormProps) => void;
}

export interface StreamFormProps {
  title: string;
  description: string;
}

const InitialState: StreamsState = {
  items: {},
  loading: true,
  error: null,
  redirectTo: null,
};

export default (
  state: StreamsState = InitialState,
  action: AllStreamActions
) => {
  switch (action.type) {
    case StreamActionTypes.List:
      return {
        items: { ...state.items, ..._.mapKeys(action.payload, "id") },
        loading: false,
        error: null,
        redirectTo: action.redirectTo,
      };
    case StreamActionTypes.Add:
    case StreamActionTypes.Edit:
    case StreamActionTypes.Fetch:
      const act = {
        items: { ...state.items, [action.payload.id]: action.payload },
        loading: false,
        error: null,
        redirectTo: action.redirectTo,
      };
      return act;
    case StreamActionTypes.Delete:
      const newItems = _.omit(state.items, action.payload);
      return {
        items: newItems,
        loading: false,
        error: null,
        redirectTo: action.redirectTo,
      };
    default:
      return state;
  }
};
