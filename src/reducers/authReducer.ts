import { AllAuthActions, AuthActions } from "../actions/auth";

export interface AuthState {
  isSignedIn: boolean | null;
  userId: string | null;
};

const InitialAuthState: AuthState = {
  isSignedIn: null,
  userId: null,
};

const authReducers = (
  state: AuthState = InitialAuthState,
  action: AllAuthActions
) => {
  switch (action.type) {
    case AuthActions.SignIn:
      return { ...state, isSignedIn: true, userId: action.payload };
    case AuthActions.SignOut:
      return { ...state, isSignedIn: false, userId: null };
    case AuthActions.Dummy:
      return state;
    default:
      return state;
  }
}

export default authReducers;
