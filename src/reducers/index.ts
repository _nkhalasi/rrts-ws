import { reducer as formReducer } from "redux-form";
import authReducer, { AuthState } from "./authReducer";
import { combineReducers } from "redux";
import streamsReducer, { StreamsState } from "./streamsReducer";

export interface ApplicationState {
  auth: AuthState;
  streams: StreamsState;
};

export default combineReducers({
  auth: authReducer,
  streams: streamsReducer,
  form: formReducer
});
