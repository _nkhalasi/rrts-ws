import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reducers, { ApplicationState } from "./reducers";
import reduxThunk, { ThunkMiddleware } from "redux-thunk";
import { ApplicationActions } from "./actions";

const store = createStore(
  reducers,
  applyMiddleware(
    reduxThunk as ThunkMiddleware<ApplicationState, ApplicationActions>
  )
);

ReactDOM.render(
  <Provider store={store}>
    <App redirectTo={null} />
  </Provider>,
  document.querySelector("#root")
);
