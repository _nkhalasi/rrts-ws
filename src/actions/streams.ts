import { ThunkAction } from "redux-thunk";
import { CrudAction, handleResponse } from "../crudutils";
import {
  Stream,
  StreamFormProps,
  StreamsState,
} from "../reducers/streamsReducer";
import backend from "../backend";
import { ApplicationState } from "../reducers";
import { formValues } from "redux-form"

export enum StreamActionTypes {
  List = 200,
  ListFailure,
  Add,
  AddFailure,
  Fetch,
  FetchFailure,
  Delete,
  DeleteFailure,
  Edit,
  EditFailure,
}

export type StreamListAction = CrudAction<StreamActionTypes.List, Stream[]>;
export type StreamAddAction = CrudAction<StreamActionTypes.Add, Stream>;
export type StreamEditAction = CrudAction<StreamActionTypes.Edit, Stream>;
export type StreamFetchAction = CrudAction<StreamActionTypes.Fetch, Stream>;
export type StreamDeleteAction = CrudAction<StreamActionTypes.Delete, number>;

export type AllStreamActions =
  | StreamListAction
  | StreamAddAction
  | StreamEditAction
  | StreamFetchAction
  | StreamDeleteAction;

export const listStreams = (): ThunkAction<
  void,
  StreamsState,
  undefined,
  StreamListAction
> => async (dispatch) => {
  handleResponse(
    await backend.get("/streams"),
    dispatch,
    StreamActionTypes.List,
    StreamActionTypes.ListFailure,
    "Could not retrieve stream list",
    null
  );
};

export const createStream = (
  formValues: StreamFormProps
): ThunkAction<void, ApplicationState, undefined, StreamAddAction> => async (
  dispatch,
  getState
) => {
    const { userId } = getState().auth;
    handleResponse(
      await backend.post("/streams", { ...formValues, userId: userId }),
      dispatch,
      StreamActionTypes.Add,
      StreamActionTypes.AddFailure,
      "Could not add stream",
      "/streams"
    );
  };

export const fetchStream = (
  id: number
): ThunkAction<void, ApplicationState, undefined, StreamAddAction> => async (
  dispatch,
  getState
) => {
    const { userId } = getState().auth;
    handleResponse(
      await backend.get(`/streams/${id}`),
      dispatch,
      StreamActionTypes.Fetch,
      StreamActionTypes.FetchFailure,
      "Could not fetch the stream",
      null
    );
  };

export const editStream = (
  id: number,
  formValues: StreamFormProps
): ThunkAction<void, ApplicationState, undefined, StreamEditAction> => async (
  dispatch,
  getState
) => {
    const { userId } = getState().auth;
    handleResponse(
      await backend.patch(`/streams/${id}`, formValues),
      dispatch,
      StreamActionTypes.Edit,
      StreamActionTypes.EditFailure,
      "Could not edit the stream",
      "/streams"
    );
  }

export const deleteStream = (
  id: number
): ThunkAction<void, ApplicationState, undefined, StreamDeleteAction> => async (
  dispatch,
  getState
) => {
    const { userId } = getState().auth;
    handleResponse(
      await backend.delete(`/streams/${id}`),
      dispatch,
      StreamActionTypes.Delete,
      StreamActionTypes.DeleteFailure,
      "Could not delete stream",
      "/streams"
    );
  };
