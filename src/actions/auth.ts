export enum AuthActions {
  SignIn = 100,
  SignOut = 101,
  Dummy = 102,
}

export interface SignInAction {
  type: AuthActions.SignIn
  payload: number
}

export interface SignOutAction {
  type: AuthActions.SignOut
}

export interface Dummy {
  type: AuthActions.Dummy
}

export type AllAuthActions = SignInAction | SignOutAction | Dummy;

export const signIn = (userId: string) => {
  return {
    type: AuthActions.SignIn,
    payload: userId
  }
}

export const signOut = () => {
  return {
    type: AuthActions.SignOut
  }
}
