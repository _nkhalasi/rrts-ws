import { AllStreamActions } from "./streams";
import { AllAuthActions } from "./auth";

export type ApplicationActions = AllStreamActions | AllAuthActions;
